# Laravel Simple Project 
A basic restful api project developed in Laravel which implements token authentication and CRUD operations. New features will be added to this project.
#Run :

install vendor :
```sh
composer install
php artisan miragrate
php artisan server
```



### Follow me on: :
* blog : [alirezap30web.ir](https://alirezap30web.ir/)
#####
* instagram : [instagram.com/alirezap30web/](https://www.instagram.com/alirezap30web/)
#####
* twitter :  [twitter.com/alirezap30web](https://twitter.com/alirezap30web)
#####
* linkedin : [linkedin.com/in/alirezap30web/](https://www.linkedin.com/in/alirezap30web/)
######
* virgool : [virgool.io/@alirezap30web](https://virgool.io/@alirezap30web)